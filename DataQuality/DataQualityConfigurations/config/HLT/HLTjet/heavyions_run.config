#**********************************************************************
# **********************************************************************

#######################
# HLTjet
#######################


#######################
# References
#######################
#The following block is kept because if signature reference is different than the centrally provided ones, the reference can be still changed
#reference HLTJetRef {
#  file = /afs/cern.ch/work/g/ggrossi/DataQuality/DataQualityConfigurations/config/HIST.09778831._000001.pool.root.1                
#  path = run_218048                                                                                                 
#  location = /eos/atlas/atlascerngroupdisk/data-dqm/references/Collisions/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
#  file = data15_13TeV.00267638.physics_EnhancedBias.merge.HIST.r6857_p1831.root
#  path = run_267638
#  name = same_name
#}


#######################
# HLTjet
#######################
#The following block is kept because if signature reference is different than the centrally provided ones, the reference can be still changed
algorithm HLTjetSimpleSummary {
  libname = libdqm_summaries.so
  name = SimpleSummary
}
compositeAlgorithm HLTjet_Histogram_Not_Empty&GatherData {
  subalgs = GatherData,Histogram_Not_Empty
  libnames = libdqm_algorithms.so
}
algorithm HLTjet_Histogram_Not_Empty&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_HardProbes:CentrallyManagedReferences_TriggerHardProbes;stream=physics_UPC:CentrallyManagedReferences_TriggerUPC;CentrallyManagedReferences_Trigger
}
algorithm HLTjet_Histogram_Not_Empty_with_Ref&GatherData {
  name = HLTjet_Histogram_Not_Empty&GatherData
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_HardProbes:CentrallyManagedReferences_TriggerHardProbes;stream=physics_UPC:CentrallyManagedReferences_TriggerUPC;CentrallyManagedReferences_Trigger 
}
compositeAlgorithm HLTjet_GatherData&Chi2Test {
  subalgs = GatherData,Chi2Test_Prob
  libnames = libdqm_algorithms.so
}
algorithm HLTjet_Chi2NDF {
  name = HLTjet_GatherData&Chi2Test_Chi2_per_NDF
  thresholds = HLTjet_Chi2_Thresh
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_HardProbes:CentrallyManagedReferences_TriggerHardProbes;stream=physics_UPC:CentrallyManagedReferences_TriggerUPC;CentrallyManagedReferences_Trigger
  MinStat = 1000
}
thresholds HLTjet_Chi2_Thresh {
  limits Chi2_per_NDFt {
    warning = 5
    error = 8
  }
}
algorithm HLTjet_KolmogorovTest_MaxDist {
  libname = libdqm_algorithms.so 
  name = KolmogorovTest_MaxDist
  thresholds = HLTjet_KolmogorovThresh
  MinStat = 100
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_HardProbes:CentrallyManagedReferences_TriggerHardProbes;stream=physics_UPC:CentrallyManagedReferences_TriggerUPC;CentrallyManagedReferences_Trigger
}
thresholds HLTjet_KolmogorovThresh {
  limits MaxDist {
    warning = 0.1
    error = 0.5
  }
}
thresholds HLTjetEta_BinsDiff_Threshold {
  limits MaxDeviation {
    warning = 3.0
    error = 6.0
  }
}
algorithm HLTjetEtaPhiAve_BinsDiff {
  libname = libdqm_algorithms.so
  name = GatherData&BinsDiffStrips
  PublishBins = 1
  MaxPublish = 10
  SigmaThresh = 0.
  xmin = -3.15
  xmax = 3.15
  MinStat = 15000
  TestConsistencyWithErrors = 0
  thresholds = HLTjetEta_BinsDiff_Threshold
  reference = stream=physics_Main:CentrallyManagedReferences_TriggerMain;stream=physics_HardProbes:CentrallyManagedReferences_TriggerHardProbes;stream=physics_UPC:CentrallyManagedReferences_TriggerUPC;CentrallyManagedReferences_Trigger
}

#######################
# Output
#######################
output top_level {
  algorithm = SimpleSummary
  output HLT {
    output TRJET {
      output Shifter {
        output Online {  
          output HLT_AntiKt4HIJets {
            output HLT_${hltChain} {
	          }
          }
          output HLT_AntiKt4EMPFlowJets_jes_ftf {
    	      output HLT_${hltChain} {
	          }
          }
        }   
      }
      output L1 {
        output ${loneHW}JetRoI {   
          output ${loneChain} {  
          }
          output NoTriggerSelection {
            output MatchedRecoJets {
            }
            output MatchedTrigJets {
            }
          } 
        }
      }
      output Expert {
        output Online {
          output HLT_AntiKt4EMPFlowJets_jes_ftf {
    	      output HLT_${hltChain} {
	          }
            output NoTriggerSelection {
              output MatchedRecoJets {
              }
              output MatchedTrigJets {
              }
            }	  	  
          }
          output HLT_AntiKt4HIJets {
    	      output HLT_${hltChain} {
	          }
            output NoTriggerSelection {
              output MatchedRecoJets {
              }
              output MatchedTrigJets {
              }
            }	  	  
          }
        } 
        output Offline {
          output AntiKt4HIJets {
          }
        } 
      }
    }
  }
}


#######################
# Histogram Assessments
#######################
dir HLT {
  dir JetMon {
    dir Online {
      algorithm     = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
      dir HLT_AntiKt4HIJets {
        regex         = 1
        display       = StatBox 
        dir HLT_(?P<hltChain>\w*) {
          regex       =  1
          hist \w*@shifter {
            description = Jets from algorithm HLT_AntiKt4HIJets, in triggered events; see twiki.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4HIJets/HLT_${hltChain}
          }
          #Expert histos
          hist \w*@expert {
            regex         = 1
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/HLT_${hltChain}
          } 
        }  
        dir NoTriggerSelection {
          regex =  1
          hist (et|pt|m|phi)(_central|_forward|)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection
          }
          hist (eta)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection
          }
          hist eta_phi {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection
            display = DRAW=COLZ
          }
          hist (eta_phi)(_pt60)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection
            display = DRAW=COLZ
          }
          hist (eta_et|phi_et|pt_m)(_central|_forward)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection
            display = DRAW=COLZ
          }
          dir MatchedJets_AntiKt4HIJets {
            regex           = 1
            hist (energydiff|energyresp|ptdiff|ptresp) {
              regex =  1
              description   = Look for changes in ${loneHW} response with respect to offline jets
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection/MatchedRecoJets
              display       = StatBox
            }
            hist (etaRef_vs_ptresp|ptRef_vs_ptresp) {
              regex =  1
              description   = Look for changes in ${loneHW} response with respect to offline jets
              output        = HLT/TRJET/Expert/Online/HLT_AntiKt4HIJets/NoTriggerSelection/MatchedRecoJets
              display       = COLZ
            }
          }
        }
      }
      dir HLT_AntiKt4EMPFlowJets_jes_ftf {
        regex         = 1
        display       = StatBox 
        dir HLT_(?P<hltChain>\w*) {
          regex       =  1
          hist \w*@shifter {
            regex =  1
            description = Jets from algorithm HLT_AntiKt4EMPFlowJets_jes_ftf, in triggered events; see twiki.
            output        = HLT/TRJET/Shifter/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/HLT_${hltChain}
          }
          #Expert histos
          hist \w*@expert {
            regex =  1
            output        = HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/HLT_${hltChain}
          }
        }   
        dir NoTriggerSelection {
          regex =  1
          hist (et|pt|m|phi)(_central|_forward|)* {
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/NoTriggerSelection
          }
          hist (eta)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/NoTriggerSelection
          }
          hist eta_phi {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/NoTriggerSelection
            display = DRAW=COLZ
          }
          hist (eta_phi)(_pt60)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/NoTriggerSelection
            display = DRAW=COLZ
          }
          hist (eta_et|phi_et|pt_m)(_central|_forward)* {
            regex =  1
            output =  HLT/TRJET/Expert/Online/HLT_AntiKt4EMPFlowJets_jes_ftf/NoTriggerSelection
            display = DRAW=COLZ
          }
        }
      }  
    }
    
    dir Offline {
      regex = 1
      description = Use reco jet response and kinematics to debug issues in trigger jets.
      algorithm   = HLTjet_Histogram_Not_Empty_with_Ref&GatherData
      dir AntiKt4HIJets {
        regex         = 1
        dir standardHistos {
          regex =  1
          hist \w* {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4HIJets
            display       = StatBox
          }
          hist .* {
            regex         = 1
            output        = HLT/TRJET/Expert/Offline/AntiKt4HIJets
            display       = StatBox
          }
        }
      }
    }  
    dir L1 {
      regex = 1
      algorithm     = HLTjet_Histogram_Not_Empty&GatherData
      dir (?P<loneHW>.*)Jet\w*  {
        regex           = 1
        dir L1_(?P<loneChain>\w*) {
          regex         = 1
          hist .* {
            regex         = 1
            description   = Check threshold and look for changes in jFex RoIs
            output        = HLT/TRJET/L1/${loneHW}JetRoI/${loneChain}
            display       = StatBox
          }
          hist (et8x8|et|eta|phi) {
            regex         = 1
            description   = Check threshold and look for changes in jFex RoIs
            output        = HLT/TRJET/L1/${loneHW}JetRoI/${loneChain}
            display       = StatBox
          }
          hist (phi_vs_eta) {
            regex         = 1
            description   = Look for changes in jFex RoIs
            output        = HLT/TRJET/L1/${loneHW}JetRoI/${loneChain}
            display       = DRAW=COLZ
          }
        }
        dir NoTriggerSelection {
          regex =  1
          hist .* {
            regex         = 1
            description   = Look for changes in ${loneHW} kinematics
            output        = HLT/TRJET/L1/${loneHW}JetRoI/NoTriggerSelection
            display       = StatBox
          }
          dir MatchedJets_AntiKt4HIJets {
            regex           = 1
            hist (energydiff|energyresp|ptdiff|ptresp) {
              description   = Look for changes in ${loneHW} response with respect to offline jets
              output        = HLT/TRJET/L1/${loneHW}JetRoI/NoTriggerSelection/MatchedRecoJets
              display       = StatBox
            }
            hist (etaRef_vs_ptresp|ptRef_vs_ptresp) {
              regex         = 1
              description   = Look for changes in ${loneHW} response with respect to offline jets
              output        = HLT/TRJET/L1/${loneHW}JetRoI/NoTriggerSelection/MatchedRecoJets
              display       = COLZ
            }
          }
          dir MatchedJets_HLT_AntiKt4HIJets {
            regex          = 1
            hist (energydiff|energyresp|ptdiff|ptresp) {
              description   = Look for changes in ${loneHW} response with respect to HLT
              output        = HLT/TRJET/L1/${loneHW}JetRoI/NoTriggerSelection/MatchedTrigJets
              display       = StatBox
            }
            hist (etaRef_vs_ptresp|ptRef_vs_ptresp) {
              regex         = 1
              description   = Look for changes in ${loneHW} response with respect to HLT
              output        = HLT/TRJET/L1/${loneHW}JetRoI/NoTriggerSelection/MatchedTrigJets
              display       = COLZ
            }
          }  
        }
      }
    }
  }
}