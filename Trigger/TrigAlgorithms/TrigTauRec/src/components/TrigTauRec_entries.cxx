#include "../TrigTauRecMerged.h"

#include "../TrigTauCaloRoiUpdater.h"
#include "../TrigTauTrackRoiUpdater.h"

DECLARE_COMPONENT( TrigTauRecMerged )

DECLARE_COMPONENT( TrigTauCaloRoiUpdater )
DECLARE_COMPONENT( TrigTauTrackRoiUpdater )
