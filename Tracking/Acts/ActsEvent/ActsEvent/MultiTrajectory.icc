/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

//  has_impl implementation
namespace ActsTrk {
namespace details {
constexpr std::optional<bool> has_impl(
    const xAOD::TrackStateAuxContainer* trackStates, Acts::HashedString key,
    ActsTrk::IndexType istate) {
  using namespace Acts::HashedStringLiteral;
  using Acts::MultiTrajectoryTraits::kInvalid;
  INSPECTCALL(key << " " << istate);

  switch (key) {
    case "previous"_hash:
      return trackStates->previous[istate] < kInvalid;
    case "chi2"_hash:{
      INSPECTCALL(key << " " << istate << " chi2");
      return true;
    }
    case "pathLength"_hash:{
      INSPECTCALL(key << " " << istate << " pathLength");
      return true;
    }
    case "typeFlags"_hash:{
      INSPECTCALL(key << " " << istate << " type flags");
      return true;
    }
    case "predicted"_hash:{
      INSPECTCALL(key << " " << istate << " predicted");
      return trackStates->predicted[istate] < kInvalid;
    }
    case "filtered"_hash:{
      INSPECTCALL(key << " " << istate << " filtered");
      return trackStates->filtered[istate] < kInvalid;
    }
    case "smoothed"_hash:{
      INSPECTCALL(key << " " << istate << " smoothed");
      return trackStates->smoothed[istate] < kInvalid;
    }
    case "jacobian"_hash:{
      INSPECTCALL(key << " " << istate << " jacobian");
      return trackStates->jacobian[istate] < kInvalid;
    }
    case "projector"_hash:{
      INSPECTCALL(key << " " << istate << " projector");
      return trackStates->calibrated[istate] < kInvalid;
    }
    case "calibrated"_hash:{
      INSPECTCALL(key << " " << istate << " calibrated");
      return trackStates->calibrated[istate] < kInvalid;
    }
    case "calibratedCov"_hash: {
      INSPECTCALL(key << " " << istate << " calibratedCov");
      return trackStates->calibrated[istate] < kInvalid;
    }
    case "measdim"_hash: {
      INSPECTCALL(key << " " << istate << " measdim");
      return trackStates->measDim[istate] < kInvalid;
    }
    case "referenceSurface"_hash: {
      INSPECTCALL(key << " " << istate << " referenceSurfaceEnco");
      return true;
    }

      // TODO restore once only the EL Source Links are in use 
      // return !trackStates[istate]->uncalibratedMeasurementLink().isDefault();
  }
  INSPECTCALL(key << " " << istate << " not a predefined component");
  return std::optional<bool>();
}
}  // namespace details
}  // namespace ActsTrk


constexpr bool ActsTrk::MutableMultiTrajectory::hasColumn_impl(
    Acts::HashedString key) const {
  using namespace Acts::HashedStringLiteral;
  INSPECTCALL(key);

  switch (key) {
    case "previous"_hash:
    case "chi2"_hash:
    case "pathLength"_hash:
    case "typeFlags"_hash:
    case "predicted"_hash:
    case "filtered"_hash:
    case "smoothed"_hash:
    case "jacobian"_hash:
    case "projector"_hash:
    case "uncalibratedSourceLink"_hash:
    case "calibrated"_hash:
    case "calibratedCov"_hash:
    case "measdim"_hash:
    case "referenceSurface"_hash:
      return true;
    default:
      for (auto& d : m_decorations) {
        if (d.hash == key) {
          return true;
        }
      }
      return false;
  }
}

template <typename T>
void ActsTrk::MutableMultiTrajectory::addColumn_impl(std::string_view name) {
  // It is actually not clear if we would allow decorating RO MTJ, maybe we do
  if constexpr (ActsTrk::detail::accepted_decoration_types<T>::value) {
    m_decorations.emplace_back( ActsTrk::detail::decoration<T>( 
        name,
        ActsTrk::detail::constDecorationGetter<T>,
        ActsTrk::detail::decorationCopier<T>,
        ActsTrk::detail::decorationSetter<T>
    )
  );
    // it would be useful to force presence of decoration already here
  } else {
    throw std::runtime_error("Can't add decoration of this type to MutableMultiTrajectory");
  }
}

template <typename val_t, typename cov_t>
void ActsTrk::MutableMultiTrajectory::allocateCalibrated_impl(IndexType istate,
                              const Eigen::DenseBase<val_t>& val,
                              const Eigen::DenseBase<cov_t>& cov)

  requires(Eigen::PlainObjectBase<val_t>::RowsAtCompileTime > 0 &&
            Eigen::PlainObjectBase<val_t>::RowsAtCompileTime <= Acts::eBoundSize &&
            Eigen::PlainObjectBase<val_t>::RowsAtCompileTime ==
                Eigen::PlainObjectBase<cov_t>::RowsAtCompileTime &&
            Eigen::PlainObjectBase<cov_t>::RowsAtCompileTime ==
                Eigen::PlainObjectBase<cov_t>::ColsAtCompileTime)
{
  constexpr std::size_t measdim = val_t::RowsAtCompileTime;

  m_trackStatesAux->measDim[istate] = measdim;
  auto idx = m_trackStatesAux->calibrated[istate];
  m_trackMeasurementsAux->meas[idx].resize(measdim);
  m_trackMeasurementsAux->covMatrix[idx].resize(measdim*measdim);

  double* measPtr = m_trackMeasurementsAux->meas[idx].data();
  Eigen::Map<Acts::ActsVector<measdim>> valMap(measPtr);
  valMap = val;

  double* covPtr = m_trackMeasurementsAux->covMatrix[idx].data();
  Eigen::Map<Acts::ActsSquareMatrix<measdim>> covMap(covPtr);
  covMap = cov;
}

